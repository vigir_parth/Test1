FROM anibali/pytorch:2.0.0-cuda11.8

USER root

WORKDIR /tmp/smp/

RUN apt-get update && apt-get install ffmpeg libsm6 libxext6  -y
RUN conda update --all -y
RUN conda install torchvision -c pytorch -c nvidia -y
RUN pip install gdown
RUN pip install git+https://github.com/qubvel/segmentation_models.pytorch.git@v0.2.1
RUN pip install -U albumentations[imgaug]
